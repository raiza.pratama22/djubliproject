module.exports = (sequelize, Sequelize) => {
const customer = sequelize.define("Customer", {
    nama_cust:{
        type:Sequelize.STRING
    },
    no_telp_cust:{
        type:Sequelize.INTEGER
    },
    jnsklmn:{
        type:Sequelize.STRING
    },
    url_photo:{
        type:Sequelize.STRING
    }
});
return customer;
}


