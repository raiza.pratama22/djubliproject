const utils = require('util');
const multer = require('multer');
const maximumsize = 2 * 1024 * 1024;
let storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, __basedir + "/app/resources/assets/");
    },
    filename: (req, file, cb) => {
        cb(null, file.originalname);
    },
});

let uploadfile = multer({
    storage: storage,
    limits: { filesize: maximumsize},
}).single("file");

let uploadfilemiddleware = utils.promisify(uploadfile);
module.exports = uploadfilemiddleware;