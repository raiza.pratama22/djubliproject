const database = require("../models");
const User = database.user;

checkExistingUsername = (req, res, next)=> {
    if(req.body.email == undefined) {
        res.status(200).send({
            status: "error",
            message: "Username required"
        })
    }
    User.findOne({
        where: {
            username: req.body.username
        }
    })
    .then(user => {
        if (user) {
            res.status(200).send({
                status: "error",
                message: "Username already used"
            });
            return;
        }
        next();
    });
};

checkExistingEmail = (req, res, next) => {
    if(req.body.email == undefined){
        res.status(200).send({
            status: "error",
            message: "Email required"
        })
    }
    User.findOne({
        where: {
            email: req.body.email
        }
    }).then(user => {
        if(user) {
            res.status(200).send({
                status: "error",
                message: "Email already used"
            });
            return;
        }
        next();
    });
};
const verifyUser = {
    checkExistingUsername: checkExistingUsername,
    checkExistingEmail: checkExistingEmail
};
module.exports = verifyUser;