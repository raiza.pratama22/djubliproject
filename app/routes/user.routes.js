const { verifyUser } = require("../middleware");
const usercontroller = require("../controllers/user.controller.js")

module.exports = function(app){
    app.use(function(req, res,next){
        res.header("Access-Control-Allow-Headers", "Authorization", "Origin, Content-Type, Accept");
        next();
    });
    app.post("/api/v1/login", usercontroller.signin);
    app.post("/api/v1/signup", [verifyUser.checkExistingUsername
    ], [verifyUser.checkExistingEmail], usercontroller.signup);

    app.post("/api/v1/login", usercontroller.signin);
}