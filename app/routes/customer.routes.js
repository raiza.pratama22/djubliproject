const { Router } = require("express");
const { jwtAuth } = require("../middleware");
module.exports = app => {
    const customer = require("../controllers/customer.controller.js");
    var route = require("express").Router();
    route.post("/",[jwtAuth.verifyToken], customer.create);
    route.put("/:id", customer.update);
    route.delete("/:id", customer.delete);
    route.delete("/", customer.deleteAll);
    route.post("/upload", customer.upload);
    route.get("/",[jwtAuth.verifyToken], customer.findAll);
    route.get("/:id", customer.findOne);
    app.use('/api/customer', route);
};