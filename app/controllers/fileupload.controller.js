const uploadFile = require("../middleware/upload");
const fs = require('fs');
const baseUrl = "https://localhost/8080/files"
const upload = async (req, res)=> {
    try{
        await uploadFile(req, res);
       if (req.file == undefined){
            return res.status(400).send({
                message: "Please upload a file!"
            });
        }
        res.status(200).send({
            message: "Upload Successfully: " + req.file.originalname,
        });

    } catch (err){
        if(err.code == "LIMIT_FILE_SIZE"){
            return res.status(500).send({
                message: "File Size cannot be larger than 2MB!",
            });
        }
       
    }
};

const getListFiles = (req, res) => {
    const directoryPath = __basedir + "/resources/assets/";
    fs.readdir(directoryPath, function(err, files){
        if (err) {
            res.status(500).send({
                message: "Unable to scan files!",
            });
        }
        let fileInfos = [];
        files.forEach((file) => {
            fileInfos.push({
                name : file,
                url: baseUrl + file,
            });
        });
        res.status(200).send(fileInfos);
    });
};

const download = (req, res) => {
    const fileName = req.params.name;
    const directoryPath = __basedir + "/resources/static/assets/uploads/";

    res.download(directoryPath + fileName, fileName, (err) => {
        if (err) {
            res.status(500).send({
                message: "Could not download file" + err,
                
            });
        }
    });
};

module.exports = {
    upload, getListFiles, download,
};