const db = require("../models");
const Customer = db.customer;
const op = db.Sequelize.op;
const uploadfile = require("../middleware/upload");
const fs = require('fs');

exports.upload = async (req, res) => {
    try{
        await uploadfile(req, res);
if(req.file == undefined){
    return res.status(400).send({
        message: "Please upload a file"
    });
}

    res.status(200).send({
        message: "Upload file Successfully" + req.file.originalname,
    });   
    }catch (err){
        res.status(500).send({
            message: "Could not upload file" + req.file.originalname + err,
        });
        if(err.code == "LIMIT_FILE_SIZE"){
            return res.status(500).send({
                message: "File size cannot larger than 2MB",
            });
        }
    }
};

exports.create = (req, res) => {
if(!req.body.nama_cust){
    res.status(400).send({
        message: "Ga bisa kosong"
    });
    return;

}
if(!req.body.no_telp_cust){
    res.status(400).send({
        message: "Ga bisa kosong"
    });
    return;
}
if(!req.body.jnsklmn){
    res.status(400).send({
        message: "Ga bisa kosong"
    });
    return;
}

const customer = {
    nama_cust: req.body.nama_cust,
    no_telp_cust: req.body.no_telp_cust,
    jnsklmn: req.body.jnsklmn

};
Customer.create(customer)
.then(data => {
    res.send(data);
}).catch(err => {
    res.status(500).send({
        message: err.message || "Some error occured"
    });
});
};

exports.update = (req, res) => {
    const id = req.params.id;
    Customer.update(req.body, {
        where: {id: id}
    }).then(num => {
        if(num === 1){
            res.send({
                message: "Customer updated!"
            });
        }else{
            res.send({
                message: `Cannot update Customer with id = ${id}`
            });
        }
    }).catch(err => {
        res.status(500).send({
            message: "Error updating customer with id=" +id
        });
    });
}
exports.delete = (req, res) => {
    const id = req.params.id;
    Customer.destroy({
        where: {id:id}
    }).then(num => {
        if(num === 1){
            res.send({
                message: "Tutorial deleted!"
            });
        }else{
            res.send({
                message: `Cannot delete Customer with id = ${id}`
            });
        }
    }).catch(err  => {
        res.status(500).send({
            message: "Could not delete Customer with id=" + id
        });
    });
};
exports.deleteAll = (req, res)=> {
    Customer.destroy({
        where: {},
        truncate: false
    }).then(nums => {
        res.send({
            message: `${nums} Customer was deleted!`
        });
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occured"
        });
    });
};
exports.findAll = (req, res) => {
    const nama_cust = req.query.nama_cust;
    var condition = nama_cust ? {nama_cust: {[op.iLike]: `%${nama_cust}%`}} : null;
    Customer.findAll({where: condition})
    .then(data => {
        res.send(data);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occured"
        });
    });

};
exports.findOne = (req, res) => {
    const id = req.params.id;
    Customer.findByPk(id).then(data => {
        if (data) {
            res.send(data);

        }else{
            res.status(404).send({
                message: ` Cannot find Customer with id=${id}`
            });
        }
    })
    .catch(err => {
        res.status(500).send({
            message: "Error retrieving Customer"
        });
    });
};